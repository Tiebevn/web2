package view;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Kleur
 */
@WebServlet("/Kleur")
public class Kleur extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Kleur() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


		PrintWriter out = response.getWriter();
		
		out.println("<!DOCTYPE HTML>");
		out.println("<html>");
		out.println("<head>");
		out.println("<title>r0663687_Kleur</title>");
		out.println("</head>");
		out.println("<body>");
		
		
		out.println("<table style='border-collapse: collapse'>");
		
		for (int i = 0; i < 360; i += 10) {
			for (int j = 0; j <= 100; j += 10) {
				
				out.println("<tr>");
				
				for (int k = 0; k <= 100; k += 10) {
					out.println("<td style='background-color: hsl(" + i + ", " + j + "%, "+ k +"%); height: 10%; width: 10% '>");
					out.println("<p style='color: #FFFFFF'>hsl(" + i + ", " + j + "%, "+ k +"%)</p>");
					out.println("</td>");
				}
				out.println("</tr>");
			}
		}
		
		out.println("</table>");
		
		out.println("</body>");
		out.println("</html>");
	}

	
}
