package view;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ValidHtmlTest {
	private WebDriver driver;
	
	@Before
	public void setUp() {
		// indien je Selenium 3.0.1 gebruikt
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Henok\\eclipse\\jee-neon\\chromedriver.exe");
		driver = new ChromeDriver();
	}
	
	@After
	public void clean() {
		driver.quit();
	}

	@Test // Voer deze test uit als je je project opgeladen hebt
	public void isValidHtml() {
		driver.get("https://html5.validator.nu/");
		// alternatieve validator
		// http://validator.w3.org/ Zoek zelf css-selector van invulveld en submit-knop
		WebElement invulveld = driver.findElement(By.id("doc"));
		// verander naamVanJeEigenSite naar de juiste naam
		invulveld.sendKeys("http://java.cyclone2.khleuven.be:38034/naamVanJeEigenSite/");
		
		WebElement button = driver.findElement(By.id("submit"));
		button.click();

		WebElement pass = driver.findElement(By.cssSelector("p.success"));
		assertEquals("The document is valid HTML5 + ARIA + SVG 1.1 + MathML 2.0 (subject to the utter previewness of this service).", pass.getText());
		
		
		
		
	}
	

}
