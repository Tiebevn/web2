package utils;

public class Kleuren {
	public static String getTabel(){
		String tabel = "<table>";
		for(int i = 0; i < 11; i++){
			for(int j = 0; j < 11; j++){
				tabel += "<tr>";
				for(int k = 0; k < 11; k++){
					tabel += "<td style='padding: 0px; margin:0px;'>"+getTegel(i*10, j*10, k*10)+"</td>";
				}
				tabel += "</tr>";
			}	
		}
		tabel += "</table>";
		return tabel;
	}
	public static String getTegel(int hue, int saturation, int light){
		String div = "<div style='background-color: hsl("+hue+", "+saturation+"%, "+light+"%);'>hsl("+hue+", "+saturation+"%, "+light+"%)</div";
		return div;
	}
}
